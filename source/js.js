// init
(() => {
  const DATA = [
    {
      src: 'assets/sport-x-max-1.png',
      like: false
    },
    {
      src: 'assets/sport-x-max-2.png',
      like: false
    },
    {
      src: 'assets/sport-x-max-3.png',
      like: false
    },
    {
      src: 'assets/sport-x-max-4.png',
      like: false
    }
  ];

  const NODES = {
    list: document.querySelector('.list'),
    preview: document.querySelector('.preview'),
    overlay: document.querySelector('.overlay'),
    controls: document.querySelector('.controls'),
    previewItem: document.querySelector('.preview-item'),
    back: document.getElementById('back'),
    like: document.getElementById('like'),
    download: document.getElementById('download')
  };

  const ITEMS = []; // list of items
  let activeId = null;

  // list item
  class Item {
    constructor(id) {
      this.id = id;
      this.data = DATA[id];
      this.self = this.create();
    }

    create() {
      const item = document.createElement('div');
      item.className = 'list-item';
      item.id = `list-item-${this.id}`;
      item.style = `background-image: url("${this.data.src}");`;
      return item;
    }

    get_params() {
      let params = this.self.getBoundingClientRect();
      return {
        w: params.width,
        h: params.height,
        l: params.left,
        t: params.top
      };
    }

    get like() {
      return this.data.like;
    }

    set like(status) {
      this.data.like = status;
    }

    get_src() {
      return this.data.src;
    }
  }

  function show_item(id) {
    activeId = id;
    let params = ITEMS[id].get_params();
    let like = ITEMS[id].like;
    let src = ITEMS[id].get_src();
    NODES.controls.classList.toggle('show');
    // set preview item
    NODES.previewItem.setAttribute(
      'style',
      `
        width: ${params.w}px;
        height: ${params.h}px;
        left: ${params.l}px;
        top: ${params.t}px;
        background-image: url('${src}');
        border-radius: 15px;
      `
    );
    setTimeout(() => {
      NODES.previewItem.style.transition = 'all 250ms ease-out';
      setTimeout(() => {
        let bodyParams = document.body.getBoundingClientRect();
        NODES.previewItem.style.width = `${bodyParams.width}px`;
        NODES.previewItem.style.height = `${bodyParams.height}px`;
        NODES.previewItem.style.left = '0';
        NODES.previewItem.style.top = '0';
        NODES.previewItem.style.borderRadius = '0';
      }, 50);
    }, 50);
    // set download link
    NODES.download.children[1].href = src;
    // set like state
    if (like) NODES.like.children[0].classList.add('liked');
    else NODES.like.children[0].classList.remove('liked');
  }

  function hideItem() {
    let params = ITEMS[activeId].get_params();
    NODES.controls.classList.toggle('show');
    // set preview item
    NODES.previewItem.style.width = `${params.w}px`;
    NODES.previewItem.style.height = `${params.h}px`;
    NODES.previewItem.style.left = `${params.l}px`;
    NODES.previewItem.style.top = `${params.t}px`;
    NODES.previewItem.style.borderRadius = '15px';
    setTimeout(() => {
      NODES.overlay.style.opacity = '';
      setTimeout(() => {
        NODES.previewItem.style.transition = '';
        NODES.preview.style.display = '';
      }, 250);
    }, 250);
    activeId = null;
  }

  // like listener
  function _likeThisShit() {
    if (activeId == null) return;
    NODES.like.children[0].classList.toggle('liked');
    ITEMS[activeId].like = !ITEMS[activeId].like;
  }

  function _back() {
    hideItem();
  }

  // list listener
  function _listClickListener(e) {
    if (e.target == NODES.list) return;
    // set item id
    let id = e.target.id;
    id = +id.slice(e.target.id.length - 1);
    // show overlay
    NODES.preview.style.display = 'block';
    setTimeout(() => {
      NODES.overlay.style.opacity = '1';
      show_item(id);
    }, 10);
  }

  // init list of items
  function loadList() {
    for (let i = 0; i < DATA.length; i++) {
      let item = new Item(i);
      ITEMS.push(item);
      NODES.list.appendChild(item.self);
    }
    NODES.list.addEventListener('click', _listClickListener);
    NODES.like.addEventListener('click', _likeThisShit);
    NODES.back.addEventListener('click', _back);
  }

  loadList();
})();
